// flow-typed signature: 59a76ba3e38d6ea7baef6365092e9f9e
// flow-typed version: <<STUB>>/shared-git-hooks_v^1.2.1/flow_v0.44.2

/**
 * This is an autogenerated libdef stub for:
 *
 *   'shared-git-hooks'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'shared-git-hooks' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'shared-git-hooks/index' {
  declare module.exports: $Exports<'shared-git-hooks'>;
}
declare module 'shared-git-hooks/index.js' {
  declare module.exports: $Exports<'shared-git-hooks'>;
}
