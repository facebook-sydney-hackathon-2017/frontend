// flow-typed signature: 2547e542d29e42367702f58a1833b751
// flow-typed version: <<STUB>>/google-libphonenumber_v^2.0.13/flow_v0.44.2

/**
 * This is an autogenerated libdef stub for:
 *
 *   'google-libphonenumber'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'google-libphonenumber' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'google-libphonenumber/dist/libphonenumber' {
  declare module.exports: any;
}

// Filename aliases
declare module 'google-libphonenumber/dist/libphonenumber.js' {
  declare module.exports: $Exports<'google-libphonenumber/dist/libphonenumber'>;
}
