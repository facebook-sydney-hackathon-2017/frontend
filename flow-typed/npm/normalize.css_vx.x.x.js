// flow-typed signature: b9e48a21a05e2e230040d7235c45387c
// flow-typed version: <<STUB>>/normalize.css_v^4.1.1/flow_v0.44.2

/**
 * This is an autogenerated libdef stub for:
 *
 *   'normalize.css'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'normalize.css' {
  declare module.exports: any;
}
