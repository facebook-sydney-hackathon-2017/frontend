#!/usr/bin/env bash

# Run web deployment script
npm run deploy:prod

# Update the s3 bucket
aws s3 rm "s3://$S3_BUCKET_NAME" --recursive
aws s3 cp ./dist/ "s3://$S3_BUCKET_NAME/" --recursive --exclude "*.map"
