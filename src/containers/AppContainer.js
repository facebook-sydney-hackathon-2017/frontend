import React, { PropTypes } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Provider } from 'react-redux'
import { Router } from 'react-router'

export default class AppContainer extends React.Component {
  render() {
    const { history, routes, routerKey, store } = this.props
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <div style={{ height: '100%' }}>
            <Router
              children={routes}
              history={history}
              key={routerKey}
            />
          </div>
        </MuiThemeProvider>
      </Provider>
    )
  }
}
AppContainer.propTypes = {
  history: PropTypes.object.isRequired,
  routerKey: PropTypes.number,
  routes: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}
