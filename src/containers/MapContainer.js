import React from 'react'
import { connect } from 'react-redux'

import acknowledgeUpdate from '../store/reducers/suburbStyles/actions/acknowledge'
import fetchSuburbs from '../store/reducers/suburbs/actions/fetch'
import fetchAverages from '../store/reducers/suburbStyles/actions/fetchAverage'
import Map from '../components/Map'

class MapContainer extends React.Component {

  /**
   * Loads the suburbs.
   */
  componentDidMount() {
    if (this.props.suburbs.suburbs === null) {
      this.props.fetchSuburbs()
    }
    this.props.fetchAverages()
  }

  /**
   * Renders the map.
   *
   * @returns {XML}
   */
  render() {
    const suburbStyles = (this.props.suburbStyles.styles || [])
    const suburbs = (this.props.suburbs.suburbs || []).map(suburb => {
      const styles = suburbStyles.find(ss => ss.id === suburb.id)
      if (typeof styles !== 'undefined') {
        return {
          ...suburb,
          ...styles
        }
      }
      return suburb
    })

    return (
      <Map
        center={this.props.map.center}
        dirty={this.props.dirty}
        fromSuburbId={this.props.suburbs.fromSuburbId}
        onReRender={this.props.acknowledgeUpdate}
        suburbs={suburbs}
      />
    )
  }
}
MapContainer.propTypes = {
  acknowledgeUpdate: React.PropTypes.func.isRequired,
  dirty: React.PropTypes.bool.isRequired,
  fetchAverages: React.PropTypes.func.isRequired,
  fetchSuburbs: React.PropTypes.func.isRequired,
  map: React.PropTypes.object.isRequired,
  suburbs: React.PropTypes.object.isRequired,
  suburbStyles: React.PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  dirty: state.suburbStyles.dirty,
  map: state.map,
  suburbs: state.suburbs,
  suburbStyles: state.suburbStyles
})

const mapDispatchToProps = {
  acknowledgeUpdate,
  fetchSuburbs,
  fetchAverages
}

export default connect(mapStateToProps, mapDispatchToProps)(MapContainer)
