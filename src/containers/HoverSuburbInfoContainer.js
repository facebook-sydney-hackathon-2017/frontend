import React from 'react'
import { connect } from 'react-redux'

import HoverSuburbInfo from '../components/HoveredSuburbInfo'

class HoverSuburbInfoContainer extends React.Component {

  render() {
    const { suburb, suburbInfo } = this.props

    if (!suburb || typeof suburb.color === 'undefined' || suburbInfo.loading) {
      return null
    }

    const duration = isNaN(this.props.suburb.average) ? this.props.suburb.duration : this.props.suburb.average
    return (
      <HoverSuburbInfo
        average={duration}
        color={this.props.suburb.color}
        destination={this.props.suburbInfo.suburb}
        name={this.props.suburb.name}
      />
    )
  }
}
HoverSuburbInfoContainer.propTypes = {
  suburb: React.PropTypes.object,
  suburbInfo: React.PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  suburb: state.suburbStyles.hoverSuburb,
  suburbInfo: state.suburbInfo
})

export default connect(mapStateToProps)(HoverSuburbInfoContainer)
