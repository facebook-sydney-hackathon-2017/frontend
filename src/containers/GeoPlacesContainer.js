import React from 'react'
import { connect } from 'react-redux'

import GeoPlacesAutocomplete from '../components/GeoPlacesAutocomplete'
import { selectSuburbByName } from '../store/reducers/suburbStyles/actions/select'

class GeoPlacesContainer extends React.Component {

  /**
   * Renders the map.
   *
   * @returns {XML}
   */
  render() {
    return (
      <GeoPlacesAutocomplete
        selectSuburbByName={this.props.selectSuburbByName}
      />
    )
  }
}
GeoPlacesContainer.propTypes = {
  selectSuburbByName: React.PropTypes.func.isRequired
}

const mapDispatchToProps = {
  selectSuburbByName
}

export default connect(null, mapDispatchToProps)(GeoPlacesContainer)
