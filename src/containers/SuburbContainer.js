import React from 'react'
import { connect } from 'react-redux'

import hoverSuburb from '../store/reducers/suburbStyles/actions/hover'
import selectSuburb from '../store/reducers/suburbStyles/actions/select'
import SuburbPolygon from '../components/SuburbPolygon'

class SuburbContainer extends React.Component {

  state = {
    selected: false
  }

  componentWillReceiveProps(nextProps) {
    if (this._isSelectedStateChanging(nextProps.selectedSuburbId)) {
      this.setState((prevState) => ({
        selected: !prevState.selected
      }))
    }
  }

  shouldComponentUpdate(nextProps) {
    return this._isSelectedStateChanging(nextProps.selectedSuburbId)
  }

  _isSelectedStateChanging = (nextSelectedSuburbId) =>
    (this.state.selected && nextSelectedSuburbId !== this.props.selectedSuburbId)
    || (!this.state.selected && nextSelectedSuburbId === this.props.selectedSuburbId)

  /**
   * Selects a suburb.
   *
   * @param suburbId
   * @private
   */
  _onSuburbClick = (suburbId) =>
    this.props.selectSuburb(suburbId)

  /**
   * Renders the polygon.
   */
  render() {
    console.log(this.props.hoverSuburb)

    return (
      <SuburbPolygon
        onClick={this._onSuburbClick}
        onHover={this.props.hoverSuburb}
        selected={this.state.selected}
        suburb={this.props.suburb}
      />
    )
  }
}
SuburbContainer.propTypes = {
  hoverSuburb: React.PropTypes.func.isRequired,
  selectedSuburbId: React.PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.string
  ]),
  selectSuburb: React.PropTypes.func.isRequired,
  suburb: React.PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  selectedSuburbId: state.suburbStyles.fromId
})

const mapDispatchToProps = {
  hoverSuburb,
  selectSuburb
}

export default connect(mapStateToProps, mapDispatchToProps)(SuburbContainer)
