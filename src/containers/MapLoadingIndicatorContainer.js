import React from 'react'
import { connect } from 'react-redux'

import classes from './MapLoadingIndicatorContainer.scss'

class MapLoadingIndicatorContainer extends React.Component {

  render() {
    if (!this.props.loading) {
      return null
    }

    return (
      <div className={classes.container}>Refreshing map...</div>
    )
  }
}
MapLoadingIndicatorContainer.propTypes = {
  loading: React.PropTypes.bool.isRequired
}

const mapStateToProps = (state) => ({
  loading: state.suburbStyles.loading
})

export default connect(mapStateToProps)(MapLoadingIndicatorContainer)
