import React from 'react'

import ErrorPage from 'components/ErrorPage'

export default class ErrorPageContainer extends React.Component {
  /**
   * Renders the error page.
   *
   * @returns {XML}
   */
  render() {
    return (
      <ErrorPage
        {...this.props}
      />
    )
  }
}
ErrorPageContainer.contextTypes = {
  layout: React.PropTypes.object.isRequired
}
ErrorPageContainer.defaultProps = {
  documentTitle: 'Error',
  message: `There's nothing here.`,
  pageTitle: 'Error'
}
