import React from 'react'
import { connect } from 'react-redux'

import Spinner from '../components/Spinner'
import SuburbInfo from '../components/SuburbInfo'

class SuburbInfoContainer extends React.Component {

  /**
   * Renders the suburb info.
   *
   * @returns {*}
   */
  render() {
    const { suburb } = this.props

    if (suburb.loading) {
      return (
        <div style={{ marginTop: '40px' }}>
          <Spinner />
        </div>
      )
    }

    if (this.props.suburb.suburb === null) {
      return null
    }

    return (
      <SuburbInfo
        suburb={this.props.suburb.suburb}
      />
    )
  }
}
SuburbInfoContainer.propTypes = {
  suburb: React.PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  suburb: state.suburbInfo
})

export default connect(mapStateToProps)(SuburbInfoContainer)
