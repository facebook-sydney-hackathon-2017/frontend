import React from 'react'

import classes from './AutocompleteItem.scss'

const AutocompleteItem = ({ formattedSuggestion }) => (
  <div>
    <span className={classes.primaryText}>{ formattedSuggestion.mainText }</span>
    <span className={classes.secondaryText}>{ formattedSuggestion.secondaryText }</span>
  </div>
)
AutocompleteItem.propTypes = {
  formattedSuggestion: React.PropTypes.object
}

export default AutocompleteItem
