import React from 'react'
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete'

import AutocompleteItem from './AutocompleteItem'

import classes from './GeoPlacesAutocomplete.scss'

export default class GeoPlacesAutocomplete extends React.Component {

  state = {
    address: ''
  }

  /**
   * Updates the address.
   *
   * @param address
   * @private
   */
  _onChange = (address) => {
    this.setState({
      address
    })
  }

  /**
   * Geocodes an address.
   *
   * @param e
   * @private
   */
  _onSubmit = (e) => {
    if (e && e.preventDefault) {
      e.preventDefault()
    }

    geocodeByAddress(this.state.address, (err, coordinates, results) => {
      if (err) {
        // todo: handle error
      }

      if (results && results.constructor === Array && results.length > 0) {
        const address = results[0]
        const locality = address.address_components.find(c => c.types.indexOf('locality') !== -1)
        if (typeof locality !== 'undefined') {
          this.props.selectSuburbByName(locality.long_name, coordinates)
        }
      }
    })
  }

  /**
   * Selects an address.
   *
   * @private
   */
  _onSelectItem = (address) => {
    this.setState({
      address
    }, this._onSubmit)
  }

  /**
   * Renders the auto complete field.
   *
   * @returns {XML}
   */
  render() {
    const classNames = {
      autocompleteContainer: classes.autocompleteContainer,
      input: classes.input,
      root: classes.root
    }

    const inputProps = {
      onChange: this._onChange,
      placeholder: 'Type an address',
      value: this.state.address
    }

    const options = {
      location: new google.maps.LatLng(-34, 151), // eslint-disable-line no-undef
      radius: 75000
    }

    return (
      <form
        className={classes.container}
        onSubmit={this._onSubmit}
      >
        <PlacesAutocomplete
          autocompleteItem={AutocompleteItem}
          classNames={classNames}
          inputProps={inputProps}
          onSelect={this._onSelectItem}
          options={options}
        />
      </form>
    )
  }
}
GeoPlacesAutocomplete.propTypes = {
  selectSuburbByName: React.PropTypes.func.isRequired
}
