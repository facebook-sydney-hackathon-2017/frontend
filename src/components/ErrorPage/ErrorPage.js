import React from 'react'

import MainLayout from 'layouts/MainLayout'

const ErrorPage = ({ children, message, title }) => (
  <MainLayout>
    {title &&
      <h1>{title}</h1>
    }
    {message &&
      <p>{message}</p>
    }
    {children}
  </MainLayout>
)
ErrorPage.propTypes = {
  children: React.PropTypes.node,
  message: React.PropTypes.string,
  title: React.PropTypes.string
}
ErrorPage.defaultProps = {
  title: '404 Not Found'
}

export default ErrorPage
