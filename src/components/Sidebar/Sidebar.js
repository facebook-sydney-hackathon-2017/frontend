import React from 'react'

import GeoPlacesAutocomplete from 'containers/GeoPlacesContainer'
import SuburbInfo from 'containers/SuburbInfoContainer'

import classes from './Sidebar.scss'

const Sidebar = () => (
  <div className={classes.container}>
    <GeoPlacesAutocomplete />
    <SuburbInfo />
  </div>
)

export default Sidebar
