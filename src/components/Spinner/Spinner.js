import React from 'react'

import './Spinner.scss'

const loadingStyle = {
  position: 'relative'
}
const svgStyle = {
  animation: 'rotate 2s linear infinite',
  margin: 'auto',
  transformOrigin: 'center center',
  width: '100%'
}
const circleStyle = {
  animation: 'dash 1.5s ease-in-out infinite',
  strokeDasharray: '1,200',
  strokeDashoffset: '0',
  strokeLinecap: 'round'
}

export default class Spinner extends React.Component {

  /**
   * Returns an object containing the properties for the type of spinner specified in props.
   *
   * @returns {{color: (String|*), diameter: Number, margin: String, thickness: Number}}
   * @private
   */
  _getType() {
    const type = {
      color: this.props.defaultColor || this.context.muiTheme.palette.primary1Color,
      diameter: this.props.diameter,
      margin: this.props.margin,
      thickness: this.props.thickness
    }

    switch (this.props.type) {
      case 'button':
        loadingStyle.display = 'inline-block'
        loadingStyle.verticalAlign = 'middle'
        type.color = this.props.defaultColor || '#fff'
        type.diameter = 25
        type.margin = ''
        type.thickness = 4
        break
      case 'medium':
        type.diameter = 30
        type.thickness = 4
        break
      case 'small':
        type.diameter = 18
        type.thickness = 3
        break
    }

    return type
  }

  /**
   * Renders the spinner.
   *
   * @returns {XML}
   */
  render() {
    const {
      className,
      style
    } = this.props

    const type = this._getType()

    loadingStyle.width = `${type.diameter}px`
    loadingStyle.height = `${type.diameter}px`
    loadingStyle.margin = type.margin

    const spinner = (
      <div style={{...loadingStyle, style}}>
        <svg
          style={svgStyle}
          viewBox='25 25 50 50'
        >
          <circle
            cx='50'
            cy='50'
            fill='none'
            r='20'
            stroke={type.color}
            strokeMiterlimit='10'
            strokeWidth={type.thickness}
            style={circleStyle}
          />
        </svg>
      </div>
    )

    return className ? (<div className={className}>{spinner}</div>) : spinner
  }
}
Spinner.contextTypes = {
  muiTheme: React.PropTypes.object.isRequired
}
Spinner.propTypes = {
  className: React.PropTypes.string,
  defaultColor: React.PropTypes.string,
  diameter: React.PropTypes.number,
  margin: React.PropTypes.string,
  style: React.PropTypes.object,
  thickness: React.PropTypes.number,
  type: React.PropTypes.oneOf([
    'button',
    'default',
    'medium',
    'small'
  ])
}
Spinner.defaultProps = {
  diameter: 40,
  margin: '0 auto',
  style: {},
  thickness: 4,
  type: 'default'
}
