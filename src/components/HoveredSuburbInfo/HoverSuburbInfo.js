import React from 'react'

import classes from './HoverSuburbInfo.scss'

const HoverSuburbInfo = ({ average, color, destination, name }) => (
  <div className={classes.container}>
    <h2>{name}</h2>
    <span
      className={classes.duration}
      style={{
        backgroundColor: color
      }}
    >
      {`${Math.ceil(average / 60)} mins`}
    </span>
    <div className={classes.destination}>
      {(typeof destination === 'undefined' || destination === null)
        ? 'avg time to anywhere in Sydney'
        : `avg time to ${destination.name}`
      }
    </div>
  </div>
)
HoverSuburbInfo.propTypes = {
  average: React.PropTypes.number.isRequired,
  color: React.PropTypes.string.isRequired,
  destination: React.PropTypes.object,
  name: React.PropTypes.string.isRequired
}

export default HoverSuburbInfo
