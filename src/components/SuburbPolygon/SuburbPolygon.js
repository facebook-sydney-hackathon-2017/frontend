import React from 'react'
import { Polygon } from 'react-google-maps'
import { connect } from 'react-redux'

import hoverSuburb from '../../store/reducers/suburbStyles/actions/hover'
import selectSuburb from '../../store/reducers/suburbStyles/actions/select'

class SuburbPolygon extends React.Component {

  state = {
    hover: false,
    selected: false
  }

  /**
   * Updates the selected state.
   *
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (this._isSelectedStateChanging(nextProps.selectedSuburbId)) {
      this.setState((prevState) => ({
        selected: !prevState.selected
      }))
    }
  }

  /**
   * Only update the component if the selected state is changing.
   *
   * @param nextProps
   */
  shouldComponentUpdate(nextProps, nextState) {
    return this._isSelectedStateChanging(nextProps.selectedSuburbId) || nextState.hover !== this.state.hover
  }

  /**
   * Determines if the selected state is changing.
   *
   * @param nextSelectedSuburbId
   * @private
   */
  _isSelectedStateChanging = (nextSelectedSuburbId) =>
    (this.state.selected && nextSelectedSuburbId !== this.props.id)
    || (!this.state.selected && nextSelectedSuburbId === this.props.id)

  /**
   * Selects a suburb.
   *
   * @param suburbId
   * @private
   */
  _onSuburbClick = (e) => {
    this.props.selectSuburb(this.props.id)
  }

  /**
   * Sets the hover state.
   *
   * @private
   */
  _onHover = () =>
    this.setState({
      hover: true
    }, () => this.props.hoverSuburb(this.props.suburb))

  /**
   * Removes the hover state.
   *
   * @private
   */
  _onMouseOut = () =>
    this.setState({
      hover: false
    })

  /**
   * Renders the polygon.
   *
   * @returns {XML}
   */
  render() {
    const defaultOptions = {
      fillColor: this.props.suburb.color,
      fillOpacity: 0.5,
      strokeWeight: 0
    }
    const selectedOptions = {
      ...defaultOptions,
      fillColor: 'black',
      fillOpacity: 0.4
    }
    const hoverOptions = {
      strokeWeight: 2
    }

    return (
      <Polygon
        {...this.props}
        onClick={this._onSuburbClick}
        onMouseout={this._onMouseOut}
        onMouseover={this._onHover}
        options={this.state.selected ? selectedOptions : this.state.hover ? hoverOptions : defaultOptions}
      />
    )
  }
}
SuburbPolygon.propTypes = {
  hoverSuburb: React.PropTypes.func.isRequired,
  id: React.PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.string
  ]),
  selectedSuburbId: React.PropTypes.oneOfType([ // eslint-disable-line
    React.PropTypes.number,
    React.PropTypes.string
  ]),
  selectSuburb: React.PropTypes.func.isRequired,
  suburb: React.PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  selectedSuburbId: state.suburbStyles.fromId
})

const mapDispatchToProps = {
  hoverSuburb,
  selectSuburb
}

export default connect(mapStateToProps, mapDispatchToProps)(SuburbPolygon)
