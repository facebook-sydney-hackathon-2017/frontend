import React from 'react'
import { GoogleMap, GoogleMapLoader } from 'react-google-maps'

import { geometryToComponentWithLatLng } from './utils'

import classes from './Map.scss'

const mapStyle = require('./mapStyle.json')

export default class Map extends React.Component {

  state = {
    suburbs: []
  }

  /**
   * Update the suburbs in state if the length of the array has changed.
   *
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.suburbs.length !== this.props.suburbs.length
      || nextProps.dirty
      || nextProps.center !== this.props.center
    ) {
      this.setState({
        suburbs: this._getShapes(nextProps.suburbs)
      })
    }
  }

  /**
   * Only update if the map if the suburbs or center has changed.
   *
   * @param nextProps
   * @param nextState
   * @returns {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.suburbs.length !== this.props.suburbs.length
      || nextProps.dirty
      || nextProps.center !== this.props.center
  }

  componentDidUpdate() {
    if (this.props.dirty) {
      this.props.onReRender()
    }
  }

  /**
   * Gets the shapes to render on the map.
   *
   * @private
   */
  _getShapes = (suburbs) => suburbs.reduce((array, suburb) => {
    const component = geometryToComponentWithLatLng(suburb.geometry)

    if (component.constructor === Array) {
      component.forEach(c => array.push(
        <c.ElementClass
          {...c}
          id={suburb.id}
          key={`${suburb.id}${Math.random()}`}
          suburb={suburb}
        />
      ))
    } else {
      array.push(
        <component.ElementClass
          {...component}
          id={suburb.id}
          key={`${suburb.id}${Math.random()}`}
          suburb={suburb}
        />
      )
    }

    return array
  }, [])

  /**
   * Renders the map.
   *
   * @returns {XML}
   * @private
   */
  _renderMap = () => {
    return (
      <GoogleMap
        defaultCenter={this.props.center}
        defaultOptions={{
          mapTypeControl: false,
          streetViewControl: false,
          styles: mapStyle
        }}
        defaultZoom={11}
      >
        {this.state.suburbs}
      </GoogleMap>
    )
  }

  /**
   * Renders the component.
   *
   * @returns {XML}
   */
  render() {
    return (
      <div className={classes.container}>
        <GoogleMapLoader
          containerElement={<div className={classes.mapContainer} />}
          googleMapElement={this._renderMap()}
        />
      </div>
    )
  }
}
Map.propTypes = {
  center: React.PropTypes.object.isRequired,
  dirty: React.PropTypes.bool.isRequired,
  onReRender: React.PropTypes.func.isRequired,
  suburbs: React.PropTypes.array.isRequired
}
