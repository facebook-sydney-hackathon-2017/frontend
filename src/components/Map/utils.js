import { InfoWindow, Marker, Polyline } from 'react-google-maps'

import SuburbPolygon from '../SuburbPolygon'

/**
 * Converts a geometry object to a google maps component.
 *
 * @param geometry
 * @returns {*}
 */
function geometryToComponentWithLatLng(geometry) {
  const typeFromThis = Array.isArray(geometry)
  const type = typeFromThis ? this.type : geometry.type
  let coordinates = typeFromThis ? geometry : geometry.coordinates

  switch (type) {
    case 'Polygon':
      return {
        ElementClass: SuburbPolygon,
        paths: coordinates.map(geometryToComponentWithLatLng, {type: 'LineString'})[0]
      }
    case 'LineString':
      coordinates = coordinates.map(geometryToComponentWithLatLng, {type: 'Point'})
      return typeFromThis ? coordinates : {
        ElementClass: Polyline,
        path: coordinates
      }
    case 'Point':
      coordinates = new google.maps.LatLng(coordinates[1], coordinates[0]) // eslint-disable-line no-undef
      return typeFromThis ? coordinates : {
        ElementClass: Marker,
        ChildElementClass: InfoWindow,
        position: coordinates
      }
    case 'MultiPolygon':
      return coordinates.map(coordinate => ({
        ElementClass: SuburbPolygon,
        paths: coordinate.map(geometryToComponentWithLatLng, {type: 'LineString'})[0]
      }))
    default:
      throw new TypeError(`Unknown geometry type: ${type}`)
  }
}

export {
  geometryToComponentWithLatLng
}
