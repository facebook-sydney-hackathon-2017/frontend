import React from 'react'

import classes from './SuburbInfo.scss'

const SuburbInfo = ({ suburb }) => (
  <div className={classes.container}>
    <Section heading title='Selected Suburb' value={suburb.name} />
    <Section title='Average Travel Time' value={`${Math.ceil(suburb.average / 60)} mins`} />
    <Section title='Median House Price' value={suburb.house_buy_median} />
    <Section title='Median House Rental' value={suburb.house_rent_median} />
    <Section title='Median Unit Price' value={suburb.unit_buy_median} />
    <Section title='Median Unit Rental' value={suburb.unit_rent_median} />
  </div>
)
SuburbInfo.propTypes = {
  suburb: React.PropTypes.object
}

export default SuburbInfo

const Section = ({ heading, title, value }) => (
  <div className={classes.section}>
    {heading ? <h2>{value}</h2> : <h3>{value}</h3>}
    <span className={classes.sectionTitle}>{title}</span>
  </div>
)
Section.propTypes = {
  heading: React.PropTypes.bool,
  title: React.PropTypes.string.isRequired,
  value: React.PropTypes.string.isRequired
}
