import MainLayout from '../layouts/MainLayout'
import Home from '../routes/Home'
import NotFoundRoute from './NotFound'

export const createRoutes = (store) => ({
  path: '/',
  component: MainLayout,
  indexRoute: Home,
  childRoutes: [
    {
      component: MainLayout,
      childRoutes: [
        NotFoundRoute(store)
      ]
    }
  ]
})

export default createRoutes
