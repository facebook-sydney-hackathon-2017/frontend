export default (store) => ({
  path: '*',

  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const NotFound = require('containers/ErrorPageContainer').default
      cb(null, NotFound)
    }, 'NotFound')
  }
})
