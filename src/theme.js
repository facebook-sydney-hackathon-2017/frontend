import {
  blue500,
  grey300,
  green200,
  green500,
  red600
} from 'material-ui/styles/colors'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

export default getMuiTheme({
  palette: {
    primary1Color: red600,
    primary2Color: red600,
    primary3Color: grey300,
    accent1Color: blue500
  },
  toggle: {
    thumbOnColor: green500,
    trackOffColor: grey300,
    trackOnColor: green200
  }
})
