import request from './config'

const getAverage = () =>
  request.get('average/all')

const getDurationFromSuburbId = (id) =>
  request.get(`duration/from/${id}`)

const getRealEstateData = (id) =>
  request.get(`realestate/${id}`)

export default {
  getAverage,
  getDurationFromSuburbId,
  getRealEstateData
}
