import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'https://api.stattrak.net/',
  timeout: 3000
})

export default axiosInstance
