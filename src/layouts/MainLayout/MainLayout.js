import React from 'react'

import HoverSuburbInfo from 'containers/HoverSuburbInfoContainer'
import MapLoadingIndicator from 'containers/MapLoadingIndicatorContainer'
import Sidebar from 'components/Sidebar'

import classes from './MainLayout.scss'

export default class MainLayout extends React.Component {
  render() {
    return (
      <div className={classes.container}>
        <Sidebar />
        <HoverSuburbInfo />
        <MapLoadingIndicator />
        {this.props.children}
      </div>
    )
  }
}
MainLayout.propTypes = {
  children: React.PropTypes.any
}
