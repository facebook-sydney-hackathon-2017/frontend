import api from 'api/suburbs'

export const actions = {
  start: 'SUBURB_INFO_FETCH_START',
  success: 'SUBURB_INFO_FETCH_SUCCESS',
  error: 'SUBURB_INFO_FETCH_ERROR'
}

export function fetchSuburbInfoStart() {
  return {
    type: actions.start
  }
}

export function fetchSuburbInfoSuccess(suburb) {
  return {
    type: actions.success,
    payload: suburb
  }
}

export function fetchSuburbInfoError(error) {
  return {
    type: actions.start,
    error
  }
}

export default function fetch(id) {
  return (dispatch) => {
    dispatch(fetchSuburbInfoStart())

    api.getRealEstateData(id)
      .then(res => {
        dispatch(fetchSuburbInfoSuccess(res.data.data))
      })
      .catch(e => dispatch(fetchSuburbInfoError(e)))
  }
}
