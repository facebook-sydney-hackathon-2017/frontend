import { actions as FETCH } from './actions/fetch'

const initialState = {
  error: null,
  loading: false,
  suburb: null
}

export default function suburbInfoReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH.start:
      return {
        ...state,
        loading: true
      }
    case FETCH.success:
      return {
        ...state,
        loading: false,
        suburb: action.payload
      }
    case FETCH.error:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    default:
      return state
  }
}
