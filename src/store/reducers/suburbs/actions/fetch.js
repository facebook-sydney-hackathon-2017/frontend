import request from 'axios'

export const actions = {
  start: 'SUBURBS_FETCH_START',
  success: 'SUBURBS_FETCH_SUCCESS',
  error: 'SUBURBS_FETCH_ERROR'
}

export function fetchSuburbsStart() {
  return {
    type: actions.start
  }
}

export function fetchSuburbsSuccess(suburbs) {
  return {
    type: actions.success,
    payload: suburbs
  }
}

export function fetchSuburbsError(error) {
  return {
    type: actions.start,
    error
  }
}

export default function fetch() {
  return (dispatch) => {
    dispatch(fetchSuburbsStart())
    request
      .get('https://d380x2ostgb46.cloudfront.net/suburbs.json')
      .then(res => {
        dispatch(fetchSuburbsSuccess(res.data.features))
      })
      .catch(e => dispatch(fetchSuburbsError(e)))
  }
}
