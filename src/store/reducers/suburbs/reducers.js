import { actions as FETCH } from './actions/fetch'

const initialState = {
  error: null,
  loading: false,
  suburbs: null
}

export default function suburbsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH.start:
      return {
        ...state,
        loading: true
      }
    case FETCH.success:
      return {
        ...state,
        loading: false,
        suburbs: action.payload
      }
    case FETCH.error:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    default:
      return state
  }
}
