export const actions = {
  start: 'SUBURB_STYLES_FETCH_START',
  success: 'SUBURB_STYLES_FETCH_SUCCESS',
  error: 'SUBURB_STYLES_FETCH_ERROR'
}

export function fetchSuburbStylesStart() {
  return {
    type: actions.start
  }
}

export function fetchSuburbStylesSuccess(suburbStyles) {
  return {
    type: actions.success,
    payload: suburbStyles
  }
}

export function fetchSuburbStylesError(error) {
  return {
    type: actions.start,
    error
  }
}

export default {
  actions,
  fetchSuburbStylesStart,
  fetchSuburbStylesSuccess,
  fetchSuburbStylesError
}
