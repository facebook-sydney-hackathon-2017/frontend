export const actions = {
  success: 'SUBURB_STYLES_UPDATE_ACKNOWLEDGE'
}

export default function acknowledgeStyleUpdate() {
  return {
    type: actions.success
  }
}
