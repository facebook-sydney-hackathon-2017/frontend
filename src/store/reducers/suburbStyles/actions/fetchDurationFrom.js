import api from 'api/suburbs'

import actions from './constants'
import { getColor } from '../utils'

export default function fetchDurationFromSuburbId(suburbId) {
  return (dispatch) => {
    dispatch(actions.fetchSuburbStylesStart())
    api.getDurationFromSuburbId(suburbId)
      .then(res => {
        if (!res.data.success) {
          return dispatch(actions.fetchSuburbStylesError(res.data.message))
        }

        const suburbStyles = res.data.data.map(suburb => ({
          ...suburb,
          color: getColor(suburb.duration)
        }))
        dispatch(actions.fetchSuburbStylesSuccess(suburbStyles))
      })
      .catch(err => dispatch(actions.fetchSuburbStylesError(err)))
  }
}
