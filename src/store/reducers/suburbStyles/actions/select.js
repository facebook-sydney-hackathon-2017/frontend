import fetchDurationFromSuburbId from './fetchDurationFrom'
import setCenter from '../../map/actions/setCenter'
import fetchSuburbInfo from '../../suburbInfo/actions/fetch'

export const actions = {
  success: 'SUBURB_SELECT'
}

export function selectSuburb(suburbId) {
  return {
    type: actions.success,
    payload: suburbId
  }
}

export default function select(suburbId) {
  return (dispatch) => {
    dispatch(fetchSuburbInfo(suburbId))
    dispatch(selectSuburb(suburbId))
    dispatch(fetchDurationFromSuburbId(suburbId))
  }
}

export function selectSuburbByName(suburbName, coordinates) {
  return (dispatch, getState) => {
    const { suburbs } = getState().suburbs

    if (suburbs && suburbs.constructor === Array) {
      const suburb = suburbs.find(s => s.name.toLowerCase() === suburbName.toLowerCase())
      if (typeof suburb === 'undefined') {
        return console.log('no suburb found')
      }
      dispatch(select(suburb.id))
      dispatch(setCenter(coordinates))
    }
  }
}
