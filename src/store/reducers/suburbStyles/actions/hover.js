export const actions = {
  success: 'SUBURB_HOVER'
}

export function hoverSuburbSuccess(suburb) {
  return {
    type: actions.success,
    payload: suburb
  }
}

export default function hoverSuburb(suburb) {
  return (dispatch, getState) => {
    const { suburbStyles } = getState()

    if (suburbStyles.styles === null) {
      return
    }

    dispatch(hoverSuburbSuccess(suburb))
  }
}
