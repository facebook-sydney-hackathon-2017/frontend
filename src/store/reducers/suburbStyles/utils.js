export const getColor = (value) => {
  switch (true) {
    case value === -1:
      return 'red'
    case value < 1000:
      return '#00FF00'
    case value < 1250:
      return '#19FF00'
    case value < 2000:
      return '#32FF00'
    case value < 2500:
      return '#4CFF00'
    case value < 2750:
      return '#65FF00'
    case value < 3250:
      return '#7FFF00'
    case value < 4250:
      return '#98FF00'
    case value < 4375:
      return '#B2FF00'
    case value < 4500:
      return '#CBFF00'
    case value < 5125:
      return '#E5FF00'
    case value < 5250:
      return '#FEFF00'
    case value < 5375:
      return '#FFE500'
    case value < 5500:
      return '#FFCC00'
    case value < 5750:
      return '#FFB200'
    case value < 6000:
      return '#FF9900'
    case value < 6250:
      return '#FF7F00'
    case value < 6500:
      return '#FF6600'
    case value < 6750:
      return '#FF4C00'
    case value < 7000:
      return '#FF3800'
    case value < 7250:
      return '#FF1900'
    default:
      return '#FF0000'
  }
}
