import { actions as ACKNOWLEDGE } from './actions/acknowledge'
import { actions as FETCH } from './actions/constants'
import { actions as HOVER } from './actions/hover'
import { actions as SELECT } from './actions/select'

const initialState = {
  dirty: false,
  error: null,
  fromId: null,
  loading: false,
  styles: [],
  toId: null
}

export default function suburbStylesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH.start:
      return {
        ...state,
        loading: true
      }
    case FETCH.success:
      return {
        ...state,
        loading: false,
        dirty: true,
        styles: action.payload
      }
    case FETCH.error:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    case SELECT.success:
      return {
        ...state,
        fromId: action.payload
      }
    case ACKNOWLEDGE.success:
      return {
        ...state,
        dirty: false
      }
    case HOVER.success:
      return {
        ...state,
        hoverSuburb: action.payload
      }
    default:
      return state
  }
}
