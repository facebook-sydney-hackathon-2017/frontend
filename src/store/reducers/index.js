import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'

import mapReducer from './map/reducers'
import suburbInfoReducer from './suburbInfo/reducers'
import suburbsReducer from './suburbs/reducers'
import suburbStylesReducer from './suburbStyles/reducers'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    // Add sync reducers here
    router,
    ...asyncReducers,
    map: mapReducer,
    suburbInfo: suburbInfoReducer,
    suburbs: suburbsReducer,
    suburbStyles: suburbStylesReducer
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
