export const actions = {
  success: 'MAP_SET_CENTER_SUCCESS'
}

export default function setMapCenter(center) {
  return {
    type: actions.success,
    payload: center
  }
}
