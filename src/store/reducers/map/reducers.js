import { actions as SET_MAP_CENTER } from './actions/setCenter'

const initialState = {
  center: {
    lat: -33.8688,
    lng: 151.2093
  }

}

export default function mapReducer(state = initialState, action) {
  switch (action.type) {
    case SET_MAP_CENTER.success:
      return {
        ...state,
        center: action.payload
      }
    default:
      return state
  }
}
