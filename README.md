# Facebook Sydney Hackathon 2017 Frontend

[Alex Price](https://gitlab.com/Outlaw11A) and I competed in Facebook's Sydney Hackathon 2017 and developed a web application for viewing the suburbs in Sydney with the best access to public transport.  We achieved 3rd place ahead of 25 other teams.  This is the frontend component of that application, built using React + Redux.

## Primary technologies
* [React](https://github.com/facebook/react) (view library)
* [Redux](https://github.com/reactjs/redux) (state management)
* [Axios](https://github.com/mzabriskie/axios) (promise-based requests)

## Issues encountered
* The initial suburbs polygons we sourced for rendering an overlay on Google Maps took 2+ seconds to render.  We resolved this by sampling each polygon at 1/8th of its original accuracy, providing a huge boost in rendering performance whilst still retaining the overall suburb shape.
* Hovering over a suburb (and changing its colour / showing its name) caused the entire map overlay to re-render.  This was resolved by managing hover state at the individual polygon component level.

## Installation
1. Clone the repository and run `npm install`
2. Run `npm start` to start a hot-reloading webpack dev server

## Deployments
* CI is setup to run lint (using ESLint), analyse (using Flow), test (using Jest) and deploy (to S3/CloudFront) stages.
* The deployment to S3/CloudFront is a one-click manual deployment on `master` only.

## Screenshot
![Screenshot](https://raw.githubusercontent.com/szdc/facebook-sydney-hackathon-2017-data/master/screenshot.png)
